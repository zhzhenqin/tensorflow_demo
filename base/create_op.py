#!/usr/bin/env python3
# -*-coding=utf-8-*-
import tensorflow as tf

# tensorflow python 库 有个 default graph ， op 构造器可以为其增加节点
# 创建一个常量 op ，产生一个 1x2 矩阵 这个 op 被作为一个节点 加载到 default graph
# op 构造器的返回值代表该常量 op 的返回值
matrix1 = tf.constant([[3., 3.]])
# 创建另外一个常量 op, 产生一个 2x1 矩阵.
matrix2 = tf.constant([[2.], [2.]])

# 创建一个矩阵乘法 matmul op 操作， 把 matrix1 和 matrix2 作为输入
# 返回值 'product'
product = tf.matmul(matrix1, matrix2)

# 现在 default graph 中包含 3个节点： 两个 constant() op 和 一个 matmul op 结果
# 现在创建 session 然后启动 这个图（default graph）
sess = tf.Session()

# 调用 sess 的 run() 方法执行矩阵乘法 op ，传入 'product' 作为该方法的参数
# 上面提到的 product 代表矩阵乘法 op 的输出，传入它是向方法表明我们希望取回其结果
result = sess.run(product)
print result

# 任务结束，关闭 session
sess.close()

with tf.Session() as sess:
    result = sess.run(product)
    print result

print "======================================="
# 通常会将一个统计模型中的参数表示为一组变量. 例如, 你可以将一个神经网络的权重
# 作为某个变量存储在一个 tensor 中. 在训练过程中, 通过重复运行训练图, 更新这个 tensor.
# 创建一个变量, 初始化为标量 0.
state = tf.Variable(0, name="counter")

# 创建一个 op, 其作用是使 state 增加 1

one = tf.constant(1)
new_value = tf.add(state, one)

# 将新的值 赋值给 state ，它 和之前 add() 方法一样，仅仅只是描绘图的操作，在 sess.run()
# 调用之前是不会执行的
update = tf.assign(state, new_value)

# 启动图后, 变量必须先经过`初始化` (init) op 初始化,
# 首先必须增加一个`初始化` op 到图中.
init_op = tf.initialize_all_variables()

# 启动图, 运行 op
with tf.Session() as sess:
    # 运行 'init' op
    sess.run(init_op)
    # 打印 'state' 的初始值
    print sess.run(state)
    # 运行 op, 更新 'state', 并打印 'state'
    for _ in range(3):
        sess.run(update)
        print sess.run(state)

        # 输出:

        # 0
        # 1
        # 2
        # 3
